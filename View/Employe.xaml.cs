﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TP_AV
{

    public partial class Employe : Window
    {
        public Employe()
        {
            InitializeComponent();
            this.DataContext = this;
        }


        private void mnuNew_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("New");
        }



        private void Open_Locaux(object sender, System.Windows.RoutedEventArgs e)
        {
            Locaux locaux = new Locaux();
            locaux.Show();
        }

        private void Open_ListeAcces(object sender, System.Windows.RoutedEventArgs e)
        {
            ListeAcces listeAcces = new ListeAcces();
            listeAcces.Show();
        }

        private void Open_Employe(object sender, System.Windows.RoutedEventArgs e)
        {
            Employe employe = new Employe();
            employe.Show();
        }


    }
}
