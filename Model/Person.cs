﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_AV.Model
{
    public class Person : INotifyPropertyChanged
    {

        private string fName;
        private string lName;
        private string numEmploye;
        private string noCarte;
        private string fullname;

        public Person(){}

        public Person(string nfName, string nlName, string nnumEmploye, string nnoCarte)
        {
            fName = nfName;
            lName = nlName;
            numEmploye = nnumEmploye;
            noCarte = nnoCarte;

        }

        public string FName
        {
            get { return fName; }
            set { fName = value; OnPropertyChanged(FName); }
        }

        public string LName
        {
            get { return lName; }
            set { lName = value; OnPropertyChanged(LName); }
        }

        public string NumEmploye
        {
            get { return numEmploye; }
            set { numEmploye = value; OnPropertyChanged(NumEmploye); }
        }

        public string NoCarte
        {
            get { return noCarte; }
            set { noCarte = value; OnPropertyChanged(NoCarte); }
        }

        public string FullName
        {
            get {
                return fullname = FName + " " + LName;
            }
            set {
                if (fullname != value){
                    fullname = value;
                }
            }
                
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string p) {
            PropertyChangedEventHandler ph = PropertyChanged;
            if (ph != null)
            {
                ph(this, new PropertyChangedEventArgs(p));
            }
                
        }
    }

}
