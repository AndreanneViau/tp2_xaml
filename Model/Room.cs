﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_AV.Model
{
    public class Room : INotifyPropertyChanged
    {

        private string numero;
        private string description;

        public Room() { }
        public Room(string nNumero, string nDescription)
        {
            numero = nNumero;
            description = nDescription;


        }

        public string Numero
        {
            get { return numero; }
            set { numero = value; OnPropertyChanged(Numero); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged(Description); }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string p)
        {
            PropertyChangedEventHandler ph = PropertyChanged;
            if (ph != null)
            {
                ph(this, new PropertyChangedEventArgs(p));
            }

        }
    }

}
